#pragma once
#include <iostream>
#include "vector"

using namespace std;

class BasePolynomial
{
    public:

        virtual void print() = 0;
        virtual int get_degree() = 0;

};
