#include "BinaryPolynomial.h"

int BinaryPolynomial::get_degree()
{
    return this->coefficients.size() - 1;
}

BinaryPolynomial::BinaryPolynomial(const BinaryPolynomial &b)
{
    for(unsigned int i = 0; i < b.coefficients.size(); i++)
        this->coefficients.push_back(b.coefficients[i]);
}

BinaryPolynomial::BinaryPolynomial(int n)
{
    bool *tab;
    tab = new bool[n + 1];

    for(int i = 0; i < n; i ++)
        tab[i] = 0;

    tab[n] = 1;

    *this = BinaryPolynomial(n, tab);
}

BinaryPolynomial::BinaryPolynomial(int degree, bool *coefficients)
{
    for(int i = 0; i <= degree; i++)
        this->coefficients.push_back(coefficients[i]);

    while(!this->coefficients[this->coefficients.size() - 1] && this->coefficients.size() > 1)
        this->coefficients.pop_back();
}

BinaryPolynomial BinaryPolynomial::operator+(BinaryPolynomial b)
{
     if(this->get_degree() >= b.get_degree())
        {
            bool *tab;
            tab = new bool[this->get_degree() + 1];

            for(int i = 0; i <= this->get_degree(); i++)
            {
                if(b.get_degree() >= i)
                    tab[i] = this->coefficients[i] ^ b.coefficients[i];
                else
                    tab[i] = this->coefficients[i];
            }

            return BinaryPolynomial(this->get_degree(), tab);
        }
        else
            return b + *this;
}

BinaryPolynomial BinaryPolynomial::operator-(BinaryPolynomial b)
{
    return *this + b;
}

BinaryPolynomial BinaryPolynomial::operator*(BinaryPolynomial b)
{
    bool tab[this->get_degree() + b.get_degree() +1];

        for(int i = 0; i <= this->get_degree() + b.get_degree(); i++)
        {
            tab[i] = 0;                                                     
        }

        for(int i = 0; i <= this->get_degree(); i++)
        {
            for(int j = 0; j <= b.get_degree(); j++)
            {
                tab[i + j] ^= this->coefficients[i] & b.coefficients[j];
            }
        }

        return BinaryPolynomial(this->get_degree() + b.get_degree(), tab);
}

BinaryPolynomial BinaryPolynomial::operator*(bool p)
{
    bool tab[this->get_degree() + 1];

    for(int i = 0; i <= this->get_degree(); i++)
        tab[i] = this->coefficients[i] * p;

    return BinaryPolynomial(this->get_degree(), tab);
}

BinaryPolynomial BinaryPolynomial::operator%(BinaryPolynomial b)
{
    if(this->get_degree() < b.get_degree())
        return *this;

    BinaryPolynomial outputBpoly(*this);

    for(int i = this->get_degree() - b.get_degree(); i >= 0; i--)
    {
		if(outputBpoly.get_degree() == i + b.get_degree())
			outputBpoly = outputBpoly + (b * BinaryPolynomial(i));
    }

    return outputBpoly;
}

BinaryPolynomial BinaryPolynomial::operator/(BinaryPolynomial b)
{
    if(this->get_degree() < b.get_degree())
        return BinaryPolynomial(0) * 0;

    BinaryPolynomial outputBpoly(*this);
    bool temp[1] = {0};
    BinaryPolynomial bpoly(0, temp);

    for(int i = this->get_degree() - b.get_degree(); i >= 0; i--)
    {
		if(outputBpoly.get_degree() == i + b.get_degree())
		{
			bpoly = bpoly + BinaryPolynomial(i);
			outputBpoly = outputBpoly + (b * BinaryPolynomial(i));
		}
    }

    return bpoly;
}

bool BinaryPolynomial::operator==(BinaryPolynomial b)
{
	BinaryPolynomial a(0);
	a = *this;
	
	do
	{
		if(a.getCoefficient(0) == b.getCoefficient(0))
		{
			if(a.get_degree() == 0)
				return true;
			else
			{			
				a = a / BinaryPolynomial(1);
				b = b / BinaryPolynomial(1);
			}
		}
		else
			return false;
	} while(a.get_degree() >= 0);
}

void BinaryPolynomial::print()
{
      for(int i = this->get_degree(); i >= 0; i--)
    {
        if(i > 0)
        {
			if(this->coefficients[i])
				cout << this->coefficients[i] << "x^" << i << " + ";
		}
		else
			cout << this->coefficients[i] << "x^" << i;
			
	}	
	cout << endl;	
}

void BinaryPolynomial::printBin()
{
      for(int i = this->get_degree(); i >= 0; i--)
    {
			cout << this->coefficients[i]<< " ";
	}	
	cout << endl;	
}

ostream& operator<<(ostream& screen, BinaryPolynomial& b)
{
	for(int i = b.get_degree(); i >= 0; i--)
	{
		if(b.coefficients[i])
			screen << b.coefficients[i] << "x^" << i << " ";
	}
	screen << endl;
		
	return screen;
}

BinaryPolynomial BinaryPolynomial::shift(int n, int k)
{
	BinaryPolynomial out(0);
	out = *this;
	
	if(n > 0)
	{
		for(int i = 0; i < n; i++)
		{
			out = out * BinaryPolynomial(1);
			if(out.get_degree() > k)
			{
				out = out - BinaryPolynomial(out.get_degree());
				out = out + BinaryPolynomial(0);
			}
		}
	}
	else
	{
		for(int i = 0; i < -n; i++)
		{
			if(out.getCoefficient(0) == 1)
			{
				out = out / BinaryPolynomial(1);
				out = out + BinaryPolynomial(out.get_degree() + 1);
			}
			else
				out = out / BinaryPolynomial(1);
		}
	}
	
	return out;
}

bool BinaryPolynomial::getCoefficient(int n)
{
	return this->coefficients[n];
}

int BinaryPolynomial::getHammingWeight()
{
	int wh;
	wh = 0;
	
	for(int i = 0; i < this->get_degree() + 1; i++)
		wh += this->getCoefficient(i);

	return wh;
}
