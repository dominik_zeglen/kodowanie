#pragma once
#include <iostream>

#include "BasePolynomial.h"

using namespace std;

class BinaryPolynomial: public BasePolynomial
{
    private:
        vector<bool> coefficients;

    public:
        int get_degree();

        BinaryPolynomial(const BinaryPolynomial&);
        BinaryPolynomial(int);
        BinaryPolynomial(int degree, bool *coefficients);

        BinaryPolynomial operator+(BinaryPolynomial);
        BinaryPolynomial operator-(BinaryPolynomial);
        BinaryPolynomial operator*(BinaryPolynomial);
        BinaryPolynomial operator*(bool);
        BinaryPolynomial operator%(BinaryPolynomial);
        BinaryPolynomial operator/(BinaryPolynomial);
        bool operator==(BinaryPolynomial);

        void print();
        void printBin();
        
        BinaryPolynomial shift(int, int);
        bool getCoefficient(int);
        int getHammingWeight();
        
        friend ostream& operator<<(ostream& screen, BinaryPolynomial& b);
};
