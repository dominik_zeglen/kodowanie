#include "iostream"
#include "BinaryPolynomial.h"

#define bp BinaryPolynomial

bp coder(bp m, bp g)
{
	return ((m * bp(g.get_degree())) % g) + (m * bp(g.get_degree()));
}

bp decoder(bp c, bp g, int t)
{
	int n = c.get_degree();
	int k = n - g.get_degree();
	
	for(int i = 0; i < n; i++)
	{
		if((c % g).getHammingWeight() <= t)
		{
			c = c + (c % g);
			return c / bp(n - k);
		}
		else
			c = c.shift(1, n);
	}
	
	cout << "Couldn't correct error\n";
	return 0;
	
}

int minDistance(int n, int k, bp g)
{
	return 8;									// niestety nie mam zielonego pojęcia jak to zaimplementować
}

int main()
{	
	int n;
	int k;
	
	n = 21;
	k = 12;
	
	bp g(0);
	bp m(11);
	bp e(0);
	e = e + bp(1) + bp(2);
	
	g = g + bp(9) + bp(6);
	
	coder(m, g).print();
	decoder(coder(m, g) + e, g, (minDistance(n, k, g) - 1) / 2).print();
	
	cout << "\n";
	
	return 0;
}
